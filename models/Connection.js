const mongoose = require ('mongoose');
const booking = new mongoose.Schema ({
    _id: mongoose.Schema.Types.ObjectId,
    product : {type: mongoose.Schema.Types.ObjectId, ref : 'Flight', required: true},
    quantity : {type: Number, default : 1},
})