const { response } = require('express');
const mongoose = require('mongoose');
const flightSchema = new mongoose.Schema ({
    destination : {
        type : String,
        required : true
    },
    status : {
        type: Boolean,
        
        default : true
    },
    price : {
        type : Number,
        required : true
    },
    date : {
        type : Date,
        required : Date.now()
    },
    bookedUser : [
        {
            userId : {
                type : String,
                required : true
            },
            status : {
                type : Boolean,
                default : true
            }
        }
    ]
});

module.exports = mongoose.model('Flight', flightSchema);